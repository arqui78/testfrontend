/**
 * ResouceController
 *
 * @description :: Server-side logic for managing Resouces
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var request = require('request');

module.exports = {
	getJSON: function(req,res,next){
	    if(!req.isSocket){
	      console.log("Obteniendo Datos desde http://socialh4ck.com/dev/jobs/test/frontend/data.json");
	      try{
	        var front = req.params.all();
	        var data = {
	          uri: 'http://socialh4ck.com/dev/jobs/test/frontend/data.json',
	          method: "GET"
	        };
	        request(data,function(error,response,body) {
	          if(!error && response.statusCode == 200){
	            var json = JSON.parse(body);
	            console.log(json);
	            res.json(200,{error: false,content:json,});
	          }else{
	            console.log("Error getJSON : "+error);
	          }
	        });
	      }catch(error){
	        console.log("ResouceController > getJSON > 0: "+error);
	        res.json({error:true,content:"Lo sentimos, hemos tenido un problema en el servidor"});
	      }
	    }
	 },
};

