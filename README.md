# testFrontend

a [Sails](http://sailsjs.org) application

Este test, a igual que el Backend, se implemento en el frameword Sails para Nodejs.

Dependencias
* npm install jade,
* npm install request,
Librerias
* angular
* lodash
* jquery
* Boostrapt
* google-maps
* font awesome icons

El test es totalmente frontend, pero a un asi, fue necesario implementar un controlador en el backend para que desde el servidor usando request obtener el recurso json desde la url indicada.

Angular se implemento de la siguinte manera:
* Modulo app principal de la aplicación.
* Un resouce.service para el manejo de la petición $http al banckend y obtener el recurso.
* Un controllador resouceController para el control de la vistas html.

Se implemento Boostrapt para los estilo y el reponsive, y algo de css con Less.

El Diseño es algo sencillo, una vista inicial, luego una sección donde se listan los clientes y una vista detalla la cual se accede al darle click al registro del cliente.

En la vista detalla, se muestra todos los datos referente al cliente y un mapa indicando su dirección el cual se genera desde sus coordenadas.

Despliegue

npm install
sails lift //Desarrollo
sails lift --prod //Produccion


 