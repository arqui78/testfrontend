(function () {
  'use strict';

  angular.module('app')
    .controller("ResouceController", ResouceController);

  ResouceController.$inject = ['$scope','$log','$timeout','resouceService'];

  function ResouceController($scope,$log,$timeout,resouceService) {

    $scope.data = {
      title   : 'CUSTOMERS',
      views :{
        home          : {active:true},
        listCustomers : {active:false},
        client        : {active:false},
      },
      back   :{},
      front  :{
        client : {},
      },
      map    : '',
      marker : '',

      //Funciones
      generateMaps  : generateMaps,
      getResouce    : getResouce,
      getTags       : getTags,
      goHome        : goHome,
      hideViews     : hideViews,
      init          : init,
      listCustomers : listCustomers,
      viewClient    : viewClint,


    };

    //Initizlice
    $scope.data.init ();

    ///////////////////////////////////

    function init (){
      $scope.data.getResouce();
    };

    function getResouce () {
      $scope.data.back.resouces = [];
      resouceService.getResouce(function(error,obj) {
        if (!error) {
          if(obj.length > 0 ) $scope.data.back.resouces = obj;
        }else $log.info(error);
      });
    };

    function goHome (){
      $scope.data.hideViews();
      $scope.data.views.home.active      = true;
    };

    function listCustomers (){
      $scope.data.hideViews();
      $scope.data.views.listCustomers.active = true;
    };

    function viewClint (client){
      $scope.data.hideViews();
      $scope.data.front.client   = [];
      $scope.data.front.client   = client;
      $scope.data.views.client.active = true;
      $scope.$watch($scope.data.front.client, function() {
        $scope.data.generateMaps();
      })

    };


    function getTags() {
      $scope.data.front.taps = [];
      angular.forEach($scope.data.front.resouces, function(resouce) {
        angular.forEach(resouce.tags, function(tags) {
          $scope.data.front.tags.push(tags);
        });
      });

      //Eliminamos los duplicados
      $scope.data.front.tags = _.uniq($scope.data.front.tags);//Libreria lodash
    };

    function hideViews(){
      $scope.data.views.home.active           = false;
      $scope.data.views.listCustomers.active  = false;
      $scope.data.views.client.active         = false;
    };

    function generateMaps () {
      var coords = {lat: $scope.data.front.client.latitude,lng:$scope.data.front.client.longitude};
      $timeout(function(){
        $scope.data.map = new google.maps.Map(document.getElementById('map'), {
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          center: coords,
          scrollwheel: false,
          zoom: 8,
        });
        var contentString =
          '<div id="content">'+
          '<div id="siteNotice">'+'</div>'+
          '<strong id="firstHeading" class="firstHeading">'+$scope.data.front.client.name+'</strong>'+
          '<div id="bodyContent">'+$scope.data.front.client.address+'</div>'+
          '<div id="bodyContent">Lat: '+coords.lat+', Lng: '+coords.lng+'</div>'+
          '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString,
          maxWidth: 300
        });

        $scope.data.marker = new google.maps.Marker({
          position: coords,
          map: $scope.data.map,
          title: $scope.data.front.client.name
        });
        infowindow.open($scope.data.map, $scope.data.marker);
        $scope.data.marker.addListener('click', function() {
          infowindow.open($scope.data.map, $scope.data.marker);
        });
        $("html,body").animate({scrollTop:$("#client").offset().top-300+"px"});
      },50,false);
    }

    $timeout(function(){
      $scope.data.getTags();
    },3000,false);

  };

})();
