(function () {

  'use strict';

  angular
    .module('app')
    .factory('resouceService',resouceService);

  resouceService.$inject = ['$http'];

  function resouceService ($http){
    var service = {
      getResouce  : getResouce
    };

    return service;

    /////////////////////

    function getResouce (cb){
      $http.get('/Resouce/GetJson')
        .then(successCallback,errorCallback);

      function successCallback(req){
        if(!req.data.error){
          cb(false,req.data.content);
        }else {
          cb(true,req.data.content);
        }
      };
      function errorCallback(error) {
        cb(true,error);
      };
    };
  };

})();
